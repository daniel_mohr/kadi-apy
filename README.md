# Kadi-APY

**Kadi-APY** is a library for use in tandem with
[Kadi4Mat](https://gitlab.com/iam-cms/kadi "Kadi4Mat"). The REST-like API of
Kadi4Mat makes it possible to programmatically interact with most of the
resources that can be used through the web interface by sending suitable HTTP
requests to the different endpoints the API provides.

The goal of this library is to make the use of this API as easy as possible. It
offers both an object oriented approach to work with the API in Python as well
as a command line interface (CLI). The library is written in Python 3 and works
under both Linux and Windows.

## Quickstart

### Installation

The library can be installed using pip:

`pip3 install kadi-apy`

Note that Python version **>=3.8** is required to install the latest version.

### Configuration

To connect to an existing Kadi4Mat instance, some further configuration is
required, which can be initialized by running:

`kadi-apy config create`

### Simple Python example

Retrieve an existing record by its ID and upload a file to it:

```python
from kadi_apy import KadiManager

manager = KadiManager()

record = manager.record(id=1)
record.upload_file("/path/to/file.txt")
```

### Simple CLI example

Do the same as in the example above:

```shell
kadi-apy records add-files -r 1 -n /path/to/file.txt
```

## Issues

For any issues regarding the kadi-apy (bugs, suggestions, discussions, etc.)
please use the [issue tracker](https://gitlab.com/iam-cms/kadi-apy/-/issues) of
this project. Before creating an issue, please make sure that no similar issue
is already open and that you are using the latest
[release](https://pypi.org/project/kadi-apy/) or the current version of the
**develop** branch. Note that creating a new issue requires a GitLab account.

For **bugs** in particular, please use the provided `Bug` template when
creating an issue, which also adds the `Bug` label to the issue automatically.

## Contributions

Contributions to the code are always welcome. In order to merge any
contributions back into the main repository, please open a corresponding [merge
request](https://gitlab.com/iam-cms/kadi-apy/-/merge_requests). Please notice
that all merge requests should go to the **develop** branch.

For instructions on how to set up a development environment, please see the
[documentation](https://kadi-apy.readthedocs.io/en/latest) for more details.

## Further links

* Source code: https://gitlab.com/iam-cms/kadi-apy
* Releases: https://pypi.org/project/kadi-apy/
* Documentation:
  * Stable (reflecting the latest release): https://kadi-apy.readthedocs.io/en/stable/
  * Latest (reflecting the *develop* branch): https://kadi-apy.readthedocs.io/en/latest/
