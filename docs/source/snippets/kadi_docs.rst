.. tip::
    Detailed information about the HTTP API of Kadi4Mat itself, including all endpoints
    and their parameters, can be found in the developer `documentation
    <https://kadi4mat.readthedocs.io/en/stable/#httpapi>`__ of Kadi4Mat.
